//:# Learn to be Swift with iOS
//: This playground will show you the basics of using Swift in playgrounds

import Cocoa // This is the OS X framework

//: Strings are easy!

let number: Int = 42
let anotherNumber = 42

let planet = "World"
var str = "Hello, \(planet)"

let array = [1, 2, 3, 4, 5]

let newArray = array.map { $0 * 2 }

for count in 0...100 {
    sin(1000.0/Double(count))
}


//: Now it's time for some fun animations!

import SpriteKit // SpriteKit is for awesome animations and games!
import XCPlayground // XCPlayground is for displaying live views

// Create a view to display our stuff
let view:SKView = SKView(frame: CGRectMake(0, 0, 320, 320))

// Create a scene
let scene:SKScene = SKScene(size: CGSizeMake(320, 320))
scene.backgroundColor = NSColor.whiteColor()
view.presentScene(scene)

// Create a simple box
let redBox = SKSpriteNode(color: NSColor.redColor(), size: CGSizeMake(100,100))
redBox.position = CGPointMake(160.0, 160.0)
scene.addChild(redBox)

redBox.runAction(SKAction.repeatActionForever(SKAction.rotateByAngle(6, duration: 2.0)))

// Create our actions
let moveAway = SKAction.moveTo(CGPointMake(260.0, 260.0), duration: 2.0)
moveAway.timingMode = .EaseInEaseOut
let moveBack = SKAction.moveTo(CGPointMake(160.0, 160.0), duration: 2.0)
moveBack.timingMode = .EaseInEaseOut

let actionSequence = [moveAway, moveBack]
let action = SKAction.repeatActionForever(SKAction.sequence(actionSequence))
redBox.runAction(action)


//Add it to the TimeLine on the right
XCPShowView("Live View", view)








